import java.util.Scanner;

public class SearchInRotatedSortedArray {
    static int x;
    static int target;
    public static int binarySearch(int[] nums, int target){
        int left = 0;
        int right = nums.length-1;
        int mid;
        while (left <= right) {
            mid = left + (right - left)/2;
            if (nums[mid] == target) return mid;
            if (nums[mid] >= nums[left]) {
                if (target >= nums[left] && target < nums[mid]) right = mid-1;
                else left = mid+1;
            } else {
                if (target <= nums[right] && target > nums[mid]) left = mid+1;
                else right = mid-1;
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        int[] num = {0,1,2,3,4,5,6};
        // rotated at pivot index 3 and become
        int[] nums = {4,5,6,0,1,2,3};
        Scanner sc = new Scanner(System.in);
        target = sc.nextInt();
        binarySearch(nums, target);
        int answer = binarySearch(nums, target);
        System.out.println("target = " + target + "\nindex = " + answer);

    }
}
