import java.util.Scanner;

public class Sqrt {
    public static int binarySearch(int x) {
        int left = 1;
        int right = x;
        
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int sqrt = x/mid;
            if (mid == sqrt ) {
                return mid; // Return the index of the target element
            } else if (mid < sqrt) {
                left = mid + 1; // Discard the left half of the array
            } else {
                right = mid - 1; // Discard the right half of the array
            }
        }

        return right; // Return -1 if the target element is not found
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        System.out.println(binarySearch(x));
    }
}
